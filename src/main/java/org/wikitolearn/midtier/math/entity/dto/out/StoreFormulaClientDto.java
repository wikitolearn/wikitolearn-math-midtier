package org.wikitolearn.midtier.math.entity.dto.out;

import lombok.Data;
import lombok.NonNull;

@Data
public class StoreFormulaClientDto {
  @NonNull
  private String formula;
  
  @NonNull
  private byte[] svg;
}
