package org.wikitolearn.midtier.math.entity.dto.out;

import lombok.Data;
import lombok.NonNull;

@Data
public class FormulaMathoidClientDto {
  @NonNull
  private String q; 
}
