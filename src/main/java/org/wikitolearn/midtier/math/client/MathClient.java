package org.wikitolearn.midtier.math.client;

import java.io.IOException;
import java.net.URI;
import java.util.concurrent.CompletableFuture;

import org.apache.commons.codec.digest.DigestUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.core.io.ByteArrayResource;
import org.springframework.core.io.Resource;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.client.HttpComponentsClientHttpRequestFactory;
import org.springframework.http.client.MultipartBodyBuilder;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;
import org.springframework.util.MimeType;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.HttpServerErrorException;
import org.springframework.web.client.ResourceAccessException;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponentsBuilder;
import org.wikitolearn.midtier.math.entity.Formula;
import org.wikitolearn.midtier.math.entity.VerifyStatus;
import org.wikitolearn.midtier.math.entity.dto.out.FormulaMathoidClientDto;
import org.wikitolearn.midtier.math.entity.dto.out.StoreFormulaClientDto;
import org.wikitolearn.midtier.math.exception.MathBackendErrorException;
import org.wikitolearn.midtier.math.exception.ResourceNotFoundException;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;

@Service
public class MathClient {

  private final RestTemplate client;

  @Value("${application.clients.math-backend}")
  private String baseUrl;

  @Autowired
  private ObjectMapper objectMapper;
  
  private static final String MATH_BACKEND_INACCESSIBLE_ERROR = "Math backend is inaccessible";
  private static final String MATH_BACKEND_SERVER_ERROR = "Math backend encountered a server error: ";
  private static final String MATH_BACKEND_CLIENT_ERROR = "Math backend encountered a client error: ";
  private static final String MATH_BACKEND_CONFLICT_ERROR = "Math backend failed to save the formula due to a conflict, but the formula does not match.";
  
  public MathClient(RestTemplateBuilder restTemplateBuilder) {
    HttpComponentsClientHttpRequestFactory requestFactory = new HttpComponentsClientHttpRequestFactory();
    client = restTemplateBuilder.requestFactory(() -> requestFactory).build();
  }

  @Cacheable(value = "formulas", keyGenerator = "formulaHashKeyGenerator")
  public Formula save(final StoreFormulaClientDto formula) throws MathBackendErrorException {
    URI uri = UriComponentsBuilder.fromHttpUrl(baseUrl + "/formulas").build().encode().toUri();

    final HttpHeaders headers = new HttpHeaders();
    headers.setContentType(MediaType.MULTIPART_FORM_DATA);

    final MultipartBodyBuilder builder = new MultipartBodyBuilder();
    ByteArrayResource svg = new ByteArrayResource(formula.getSvg()) {
      @Override
      public String getFilename() {
        return DigestUtils.sha256Hex(formula.getFormula()) + ".svg";
      }
    };

    builder.part("svg", svg, MediaType.asMediaType(MimeType.valueOf("image/svg+xml")));
    builder.part("formula", formula.getFormula());

    final MultiValueMap<String, HttpEntity<?>> multipartBody = builder.build();
    final HttpEntity<MultiValueMap<?, ?>> httpEntity = new HttpEntity<>(multipartBody, headers);

    try {
      return client.postForObject(uri, httpEntity, Formula.class);
    } catch (HttpClientErrorException e) {
      // If the formula is already stored, check the formula string and then return it
      if (HttpStatus.CONFLICT.value() == e.getRawStatusCode()) {
        uri = UriComponentsBuilder.fromUriString(uri.toString() + "/{id}")
            .buildAndExpand(DigestUtils.sha256Hex(formula.getFormula())).encode().toUri();

        final Formula alreadyStoredFormula = client.getForObject(uri, Formula.class);

        if (formula.getFormula().equals(alreadyStoredFormula.getFormula())) {
          return alreadyStoredFormula;
        } else {
          throw new MathBackendErrorException(MATH_BACKEND_CONFLICT_ERROR, e); 
        }
      }
      throw new MathBackendErrorException(MATH_BACKEND_CLIENT_ERROR + e.getStatusText(), e);
    } catch (HttpServerErrorException e) {
      throw new MathBackendErrorException(MATH_BACKEND_SERVER_ERROR + e.getStatusText(), e);
    } catch (ResourceAccessException e) {
      throw new MathBackendErrorException(MATH_BACKEND_INACCESSIBLE_ERROR, e);
    }
  }

  @Cacheable(value = "formulas", key = "#id")
  public Formula get(final String id) throws ResourceNotFoundException, MathBackendErrorException {
    final URI uri = UriComponentsBuilder.fromHttpUrl(baseUrl + "/formulas/{id}").buildAndExpand(id).encode().toUri();
    try {
      return client.getForObject(uri, Formula.class);
    } catch (HttpClientErrorException e) {
      if (HttpStatus.NOT_FOUND.equals(e.getStatusCode())) {
        throw new ResourceNotFoundException(MATH_BACKEND_CLIENT_ERROR + e.getStatusText(), e);
      }
      throw new MathBackendErrorException(MATH_BACKEND_CLIENT_ERROR + e.getStatusText(), e);
    } catch (HttpServerErrorException e) {
      throw new MathBackendErrorException(MATH_BACKEND_SERVER_ERROR + e.getStatusText(), e);
    } catch (ResourceAccessException e) {
      throw new MathBackendErrorException(MATH_BACKEND_INACCESSIBLE_ERROR, e);
    }
  }

  @Cacheable(value = "svg-media", key = "#mediaId")
  public Resource getSVG(final String mediaId) throws ResourceNotFoundException, MathBackendErrorException {
    final URI uri = UriComponentsBuilder.fromHttpUrl(baseUrl + "/rendered-formulas/{mediaId}").buildAndExpand(mediaId)
        .encode().toUri();
    try {
      return client.getForObject(uri, Resource.class);
    } catch (HttpClientErrorException e) {
      if (HttpStatus.NOT_FOUND.equals(e.getStatusCode())) {
        throw new ResourceNotFoundException(MATH_BACKEND_CLIENT_ERROR + e.getStatusText(), e);
      }
      throw new MathBackendErrorException(MATH_BACKEND_CLIENT_ERROR + e.getStatusText(), e);
    } catch (HttpServerErrorException e) {
      throw new MathBackendErrorException(MATH_BACKEND_SERVER_ERROR + e.getStatusText(), e);
    } catch (ResourceAccessException e) {
      throw new MathBackendErrorException(MATH_BACKEND_INACCESSIBLE_ERROR, e);
    }
  }

  @Cacheable(value = "svgs", key = "#formula.formula")
  public byte[] generateSVG(final Formula formula) throws MathBackendErrorException {
    final URI uri = UriComponentsBuilder.fromHttpUrl(baseUrl + "/mathoid/svg").build().encode().toUri();

    final HttpHeaders headers = new HttpHeaders();
    headers.setContentType(MediaType.APPLICATION_JSON_UTF8);
    headers.add("Accept", "image/svg+xml");

    final HttpEntity<FormulaMathoidClientDto> httpEntity = new HttpEntity<FormulaMathoidClientDto>(
        new FormulaMathoidClientDto(formula.getFormula()), headers);
    
    try {
      return client.postForObject(uri, httpEntity, byte[].class);
    } catch (HttpClientErrorException e) {
      throw new MathBackendErrorException(MATH_BACKEND_CLIENT_ERROR + e.getStatusText(), e);
    } catch (HttpServerErrorException e) {
      throw new MathBackendErrorException(MATH_BACKEND_SERVER_ERROR + e.getStatusText(), e);
    } catch (ResourceAccessException e) {
      throw new MathBackendErrorException(MATH_BACKEND_INACCESSIBLE_ERROR, e);
    }
  }

  @Async
  @Cacheable(value = "mathoid-checks", key = "#formula.formula")
  public CompletableFuture<VerifyStatus> verify(final Formula formula)
      throws MathBackendErrorException, JsonParseException, JsonMappingException, IOException {
    final URI uri = UriComponentsBuilder.fromHttpUrl(baseUrl + "/mathoid/texvcinfo").build().encode().toUri();
    final HttpHeaders headers = new HttpHeaders();
    headers.setContentType(MediaType.APPLICATION_JSON_UTF8);

    final HttpEntity<FormulaMathoidClientDto> httpEntity = new HttpEntity<FormulaMathoidClientDto>(
        new FormulaMathoidClientDto(formula.getFormula()), headers);

    try {
      final VerifyStatus verifyStatus = client.postForObject(uri, httpEntity, VerifyStatus.class);
      return CompletableFuture.completedFuture(verifyStatus);
    } catch (HttpClientErrorException e) {
      if (HttpStatus.BAD_REQUEST.equals(e.getStatusCode())) {
        final VerifyStatus status = objectMapper.readValue(e.getResponseBodyAsString(), VerifyStatus.class);
        return CompletableFuture.completedFuture(status);
      }
      throw new MathBackendErrorException(MATH_BACKEND_CLIENT_ERROR + e.getStatusText(), e);
    } catch (HttpServerErrorException e) {
      throw new MathBackendErrorException(MATH_BACKEND_SERVER_ERROR + e.getStatusText(), e);
    } catch (ResourceAccessException e) {
      throw new MathBackendErrorException(MATH_BACKEND_INACCESSIBLE_ERROR, e);
    }
  }

}
