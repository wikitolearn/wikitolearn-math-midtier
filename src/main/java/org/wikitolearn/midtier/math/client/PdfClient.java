package org.wikitolearn.midtier.math.client;

import java.io.IOException;
import java.net.URI;
import java.util.concurrent.CompletableFuture;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.client.HttpComponentsClientHttpRequestFactory;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.HttpServerErrorException;
import org.springframework.web.client.ResourceAccessException;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponentsBuilder;
import org.wikitolearn.midtier.math.entity.Formula;
import org.wikitolearn.midtier.math.entity.VerifyStatus;
import org.wikitolearn.midtier.math.entity.dto.in.StoreFormulaDto;
import org.wikitolearn.midtier.math.exception.PdfBackendErrorException;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;

@Service
public class PdfClient {

  private final RestTemplate client;

  @Value("${application.clients.pdf-backend}")
  private String baseUrl;

  @Autowired
  private ModelMapper modelMapper;

  @Autowired
  private ObjectMapper objectMapper;

  private static final String PDF_BACKEND_INACCESSIBLE_ERROR = "PDF backend is inaccessible";
  private static final String PDF_BACKEND_SERVER_ERROR = "PDF backend encountered a server error: ";
  private static final String PDF_BACKEND_CLIENT_ERROR = "PDF backend encountered a client error: ";

  public PdfClient(RestTemplateBuilder restTemplateBuilder) {
    HttpComponentsClientHttpRequestFactory requestFactory = new HttpComponentsClientHttpRequestFactory();
    client = restTemplateBuilder.requestFactory(() -> requestFactory).build();
  }

  @Cacheable(value = "pdf-inline-checks", key = "#formula.formula")
  public CompletableFuture<VerifyStatus> verifyInline(final Formula formula)
      throws PdfBackendErrorException, JsonParseException, JsonMappingException, IOException {
    final URI uri = UriComponentsBuilder.fromHttpUrl(baseUrl + "/math-verifier/inline").build().encode().toUri();
    return verify(uri, formula);
  }

  @Cacheable(value = "pdf-block-checks", key = "#formula.formula")
  public CompletableFuture<VerifyStatus> verifyBlock(final Formula formula)
      throws PdfBackendErrorException, JsonParseException, JsonMappingException, IOException {
    final URI uri = UriComponentsBuilder.fromHttpUrl(baseUrl + "/math-verifier/inline").build().encode().toUri();
    return verify(uri, formula);
  }

  @Async
  private CompletableFuture<VerifyStatus> verify(final URI uri, final Formula formula)
      throws PdfBackendErrorException, JsonParseException, JsonMappingException, IOException {
    final HttpHeaders headers = new HttpHeaders();
    headers.setContentType(MediaType.APPLICATION_JSON_UTF8);

    final HttpEntity<StoreFormulaDto> httpEntity = new HttpEntity<StoreFormulaDto>(
        modelMapper.map(formula, StoreFormulaDto.class), headers);

    try {
      final VerifyStatus status = client.postForObject(uri, httpEntity, VerifyStatus.class);
      return CompletableFuture.completedFuture(status);
    } catch (HttpClientErrorException e) {
      if (HttpStatus.BAD_REQUEST.equals(e.getStatusCode())) {
        final VerifyStatus status = objectMapper.readValue(e.getResponseBodyAsString(), VerifyStatus.class);
        return CompletableFuture.completedFuture(status);
      }
      throw new PdfBackendErrorException(PDF_BACKEND_CLIENT_ERROR + e.getStatusText(), e);
    } catch (HttpServerErrorException e) {
      throw new PdfBackendErrorException(PDF_BACKEND_SERVER_ERROR + e.getStatusText(), e);
    } catch (ResourceAccessException e) {
      throw new PdfBackendErrorException(PDF_BACKEND_INACCESSIBLE_ERROR, e);
    }
  }

}
