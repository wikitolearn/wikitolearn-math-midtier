package org.wikitolearn.midtier.math;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class MathMidTierApplication {

	public static void main(String[] args) {
		SpringApplication.run(MathMidTierApplication.class, args);
	}
}
