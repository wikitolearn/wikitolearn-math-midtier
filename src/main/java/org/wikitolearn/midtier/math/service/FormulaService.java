package org.wikitolearn.midtier.math.service;

import java.io.IOException;
import java.util.concurrent.CompletableFuture;
import java.util.stream.Stream;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.Resource;
import org.springframework.stereotype.Service;
import org.wikitolearn.midtier.math.client.MathClient;
import org.wikitolearn.midtier.math.client.PdfClient;
import org.wikitolearn.midtier.math.entity.Formula;
import org.wikitolearn.midtier.math.entity.VerifyStatus;
import org.wikitolearn.midtier.math.entity.dto.out.StoreFormulaClientDto;
import org.wikitolearn.midtier.math.exception.BackendErrorException;
import org.wikitolearn.midtier.math.exception.InvalidFormulaException;
import org.wikitolearn.midtier.math.exception.MathBackendErrorException;
import org.wikitolearn.midtier.math.exception.ResourceNotFoundException;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;

@Service
public class FormulaService {

  @Autowired
  private MathClient mathClient;

  @Autowired
  private PdfClient pdfClient;
  
  private static final String INVALID_FORMULA_ERROR = "The formula provided is invalid and can not be stored";

  public Formula save(final Formula formula) throws JsonParseException, JsonMappingException, InvalidFormulaException, BackendErrorException, IOException {
    if (isValidFormula(formula)) {
      final byte[] generatedSVG = mathClient.generateSVG(formula);
      final StoreFormulaClientDto formulaToStore = new StoreFormulaClientDto(formula.getFormula(), generatedSVG);
      return mathClient.save(formulaToStore);
    } else {
      throw new InvalidFormulaException(INVALID_FORMULA_ERROR);
    }
  }
  
  public Formula get(final String id) throws ResourceNotFoundException, MathBackendErrorException {
    return mathClient.get(id);
  }
  
  public Resource getSVG(final String id) throws ResourceNotFoundException, MathBackendErrorException{
     final Formula formula = get(id);
     final String mediaId = StringUtils.substringAfterLast(formula.getSvg().getFile(), "/");
     return mathClient.getSVG(mediaId);
  }
  
  public VerifyStatus check(final Formula formula) throws BackendErrorException, JsonParseException, JsonMappingException, IOException {
    return new VerifyStatus(isValidFormula(formula));
  }
  
  private boolean isValidFormula(final Formula formula) throws BackendErrorException, JsonParseException, JsonMappingException, IOException {
    final CompletableFuture<VerifyStatus> mathoidStatus = mathClient.verify(formula);
    final CompletableFuture<VerifyStatus> pdfInlineStatus = pdfClient.verifyInline(formula);
    final CompletableFuture<VerifyStatus> pdfBlockStatus = pdfClient.verifyBlock(formula);
    
    final boolean result = Stream.of(mathoidStatus, pdfInlineStatus, pdfBlockStatus)
        .map(CompletableFuture::join)
        .allMatch(r -> r.isSuccess() == true);
    
    return result;
  }
}
